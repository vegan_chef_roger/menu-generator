from docx import Document
import random
import datetime
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive


class Menu:

    def __init__(self):
        self.home = "/home/b134ch3473r/Documents/Radical_Roots/"
        self.saveDir = self.home + "weekly/"
        self.saveName = "weekly_menu.docx"
        self.savePath = self.saveDir + self.saveName
        self.menu = self.home + "template.docx"
        self.items = self.home + "items.txt"
        self.item1 = None
        self.item2 = None
        self.item3 = None
        self.startDate = None
        self.endDate = None
        self.file1ID = "1c2CTg76ptaKhZ9VfvqD1Zf-rKZUhsNjiicV6fiYwMkY"

    def uploadFile(self):
        #do oauth
        gauth = GoogleAuth()
        gauth.LocalWebserverAuth()
        drive = GoogleDrive(gauth)
        #upload file
        file1 = drive.CreateFile({'id': str(self.file1ID)})
        file1.SetContentFile(self.savePath)
        file1.Upload()

    def getDates(self):
        delta = datetime.timedelta(days=10)
        today = datetime.date.today()
        start = today + delta
        self.startDate = start.strftime('%b %d')
        delta2 = datetime.timedelta(days=7)
        end = start + delta2
        self.endDate = end.strftime('%b %d')

    def getRandomItems(self):
        numLines = sum(1 for line in open(self.items))
        itemNumbers = random.sample(list(range(1, numLines)), 3)
        #print(itemNumbers)
        self.item1 = self.getItem(itemNumbers.pop())
        #print(self.item1)
        #print(itemNumbers)
        self.item2 = self.getItem(itemNumbers.pop())
        #print(self.item2)
        #print(itemNumbers)
        self.item3 = self.getItem(itemNumbers.pop())
        #print(self.item3)
        #print(itemNumbers)

    def getItem(self, int1):
        int2 = 1
        with open(self.items, "r") as items:
            for line in items:
                line = str(line).rstrip()
                if int1 == int2:
                    return line.split("||")
                int2 = int2 + 1

    def insertItems(self):
        document = Document(self.menu)
        for paragraph in document.paragraphs:
            if "$$$ITEM1$$$" in paragraph.text:
                #print(paragraph.text)
                paragraph.text = str(self.item1[0]) + ": "
                #print(str(replace[0]))
                run = paragraph.runs[0]
                run.font.bold = True
                run2 = paragraph.add_run()
                run2.font.bold = False
                run2.text = self.item1[1]
            elif "$$$ITEM2$$$" in paragraph.text:
                #print(paragraph.text)
                paragraph.text = str(self.item2[0]) + ": "
                #print(str(replace[0]))
                run = paragraph.runs[0]
                run.font.bold = True
                run2 = paragraph.add_run()
                run2.font.bold = False
                run2.text = self.item2[1]
            elif "$$$ITEM3$$$" in paragraph.text:
                #print(paragraph.text)
                paragraph.text = str(self.item3[0]) + ": "
                #print(str(replace[0]))
                run = paragraph.runs[0]
                run.font.bold = True
                run2 = paragraph.add_run()
                run2.font.bold = False
                run2.text = self.item3[1]
            elif "$$$DATE$$$" in paragraph.text:
                #print(paragraph.text)
                paragraph.text = "Specials for: " + str(self.startDate) + " to " + str(self.endDate)
                #print(str(replace[0]))
                run = paragraph.runs[0]
                run.font.bold = True
        document.save(self.savePath)


menu = Menu()
menu.getDates()
menu.getRandomItems()
menu.insertItems()
menu.uploadFile()